import os
import json
import hmac
import base64
import requests
import hashlib
from datetime import datetime

def process_event(helper, *args, **kwargs):
    
    appname = helper.get_global_setting("appname")
    appkey = helper.get_global_setting("appkey")
    secretkey = helper.get_global_setting("secretkey")
    
    TIME_STAMP_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
    
    class Credentials(object):
        """The authorization of transaction repository api."""
    
        def __init__(self, app_name, app_key, secret_key):
            """Initialize client class."""
            self.app_name = app_name
            self.app_key = app_key
            self.secret_key = secret_key
    
        def _get_valid_datetime(self):
            """Returns the current utc datetime in the correct timestamp"""
            now = datetime.utcnow()
            return now.strftime(TIME_STAMP_FORMAT)
    
        def build_token(self, token_datetime=None):
            """Build transaction repository api authorization token."""
            token_datetime = token_datetime or self._get_valid_datetime()
    
            raw_data = f"{self.app_name}{token_datetime}".upper()
            digestor = hmac.new(self.secret_key.encode(),
                                raw_data.encode(), hashlib.sha256)
            signature = base64.encodebytes(digestor.digest())
            signature = signature.decode().replace("\n", "")
            return f"{self.app_key};{signature};{token_datetime}"
    
    """Generate authorization token"""
    credential = Credentials(appname, appkey, secretkey)
    token = credential.build_token()
    helper.log_info(f"{token}")
    
    """Post arguments"""
    environmentUrl = helper.get_global_setting("environmenturl") + "/alert-execution-publisher"
    alertid = helper.get_param("alertid")
    executionmethod = helper.get_param("executionmethod")
    stonecode = helper.get_param("stonecode")
    clientdocumentnumber = helper.get_param("clientdocumentnumber")
    ravip = helper.get_param("ravip")
    ravcityip = helper.get_param("ravcityip")
    ravstateip = helper.get_param("ravstateip")
    ravdevice = helper.get_param("ravdevice")
    clientcity = helper.get_param("clientcity")
    clientstate = helper.get_param("clientstate")
    contastone = helper.get_param("contastone")
    referencedate = helper.get_param("referencedate")
    
    data = f'{{ "alertId": "{alertid}", "executionMethod": {{"id": 1, "name": "Scheduled"}}, "executionTimestamp": "{datetime.utcnow().strftime(TIME_STAMP_FORMAT)}", "merchantIdentifier": "{stonecode}", "metadata": "{{\\"[INT] clientdocumentnumber\\": \\"{clientdocumentnumber}\\", \\"[STR] ravip\\": \\"{ravip}\\", \\"[STR] ravcityip\\": \\"{ravcityip}\\", \\"[STR] ravstateip\\": \\"{ravstateip}\\", \\"[STR] ravdevice\\": \\"{ravdevice}\\", \\"[STR] clientcity\\": \\"{clientcity}\\", \\"[STR] clientstate\\": \\"{clientstate}\\", \\"[STR] contastone\\": \\"{contastone}\\" }}", "referenceDate": "{referencedate}" }}'
    helper.log_info(f"{data}")

    try:
        """Post"""
        headers = {
            'Authorization': token,
            'Content-Type': 'application/json',
        }
        helper.log_info(f"{headers}")
        r = requests.post(environmentUrl, headers=headers, data=data, timeout=30)
        helper.log_info(f"{r.text}")
    except requests.exceptions.RequestException as e:
        helper.log_error(f"{e}")
        return
    return