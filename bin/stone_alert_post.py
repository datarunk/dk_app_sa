
# encoding = utf-8
# Always put this line at the beginning of this file
import dk_app_sa_declare

import os
import sys

from alert_actions_base import ModularAlertBase
import modalert_stone_alert_post_helper

class AlertActionWorkerstone_alert_post(ModularAlertBase):

    def __init__(self, ta_name, alert_name):
        super(AlertActionWorkerstone_alert_post, self).__init__(ta_name, alert_name)

    def validate_params(self):

        if not self.get_global_setting("environmenturl"):
            self.log_error('environmenturl is a mandatory setup parameter, but its value is None.')
            return False

        if not self.get_global_setting("appname"):
            self.log_error('appname is a mandatory setup parameter, but its value is None.')
            return False

        if not self.get_global_setting("appkey"):
            self.log_error('appkey is a mandatory setup parameter, but its value is None.')
            return False

        if not self.get_global_setting("secretkey"):
            self.log_error('secretkey is a mandatory setup parameter, but its value is None.')
            return False

        if not self.get_param("alertid"):
            self.log_error('alertid is a mandatory parameter, but its value is None.')
            return False

        if not self.get_param("executionmethod"):
            self.log_error('executionmethod is a mandatory parameter, but its value is None.')
            return False

        if not self.get_param("referencedate"):
            self.log_error('referencedate is a mandatory parameter, but its value is None.')
            return False

        if not self.get_param("stonecode"):
            self.log_error('stonecode is a mandatory parameter, but its value is None.')
            return False
        return True

    def process_event(self, *args, **kwargs):
        status = 0
        try:
            if not self.validate_params():
                return 3
            status = modalert_stone_alert_post_helper.process_event(self, *args, **kwargs)
        except (AttributeError, TypeError) as ae:
            self.log_error("Error: {}. Please double check spelling and also verify that a compatible version of Splunk_SA_CIM is installed.".format(str(ae)))
            return 4
        except Exception as e:
            msg = "Unexpected error: {}."
            if e:
                self.log_error(msg.format(str(e)))
            else:
                import traceback
                self.log_error(msg.format(traceback.format_exc()))
            return 5
        return status

if __name__ == "__main__":
    exitcode = AlertActionWorkerstone_alert_post("dk_app_sa", "stone_alert_post").run(sys.argv)
    sys.exit(exitcode)
