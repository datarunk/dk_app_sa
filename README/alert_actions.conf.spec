

[stone_alert_post]
python.version = python3
param.alertid = <string> alertid. It's a required parameter.
param.executionmethod = <string> executionMethod. It's a required parameter. It's default value is {"id": 1, "name": "Scheduled"}.
param.referencedate = <string> referenceDate. It's a required parameter.
param.stonecode = <string> StoneCode. It's a required parameter.
param.clientdocumentnumber = <string> ClientDocumentNumber.
param.ravip = <string> RavIP.
param.ravcityip = <string> RavCityIP.
param.ravstateip = <string> RavStateIP.
param.ravdevice = <string> RavDevice.
param.clientcity = <string> ClientCity.
param.clientstate = <string> ClientState.
param.contastone = <string> ContaStone.

